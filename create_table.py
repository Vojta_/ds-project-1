#!/usr/bin/python

import psycopg2
from config import config


def create_tables():
    """ create tables in the PostgreSQL database"""
    commands = (
        """
        CREATE TABLE department (
            id SERIAL PRIMARY KEY,
            name VARCHAR(40) NOT NULL,
            study_programs VARCHAR(40) NOT NULL,
            head_of_department VARCHAR(40) NOT NULL,
            employees VARCHAR(40) NOT NULL)""",


        """ 
        CREATE TABLE study_program  (
                study_program_id SERIAL PRIMARY KEY,
                name VARCHAR(20) NOT NULL
                supervisor VARCHAR(20) NOT NULL
                department_id INTEGER NOT NULL)""",


        """
        CREATE TABLE student (
                student_id INTEGER PRIMARY KEY,
                first_name VARCHAR(10) NOT NULL,
                last_name  VARCHAR(10) NOT NULL,
                contact VARCHAR(10) NOT NULL)""",

        """
        CREATE TABLE employee (
                employees_id INTEGER PRIMARY KEY,
                first_name VARCHAR(10) NOT NULL,
                last_name VARCHAR(10) NOT NULL,
                contact VARCHAR(10) NOT NULL.
                office VARCHAR(10) NOT NULL,
                department VARCHAR(20) NOT NULL)""",
        
        
        """
        CREATE TABLE head_of_department (
                head_department_id INTEGER PRIMARY KEY,
                first_name VARCHAR(10) NOT NULL,
                last_name VARCHAR(10) NOT NULL,
                contact VARCHAR(10) NOT NULL.
                office VARCHAR(10) NOT NULL)""",

        """
        CREATE TABLE course (
                course_id INTEGER PRIMARY KEY,
                employee_id INTEGER(10) NOT NULL,
                name VARCHAR(10) NOT NULL)""",                
                
                
                
    
    
    
    )#Konec commands
    
    conn = None
    try:
        # read the connection parameters
        params = config()
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    create_tables()